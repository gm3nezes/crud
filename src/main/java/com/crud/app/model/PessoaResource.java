package com.crud.app.model;

import java.util.List;
import java.util.Optional;

import javax.persistence.OrderColumn;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pessoas")
public class PessoaResource {
	
	@Autowired
	private PessoaRepository pr;
	
	@GetMapping()
	public List<Pessoa> listar() {
		return (List<Pessoa>) pr.findAll();
	}
	
	@GetMapping("/{id}")
	public Pessoa listaPessoa(@PathVariable("id") int id) {
		return pr.findById(id).get();
	}
	
	@PostMapping("/add")
	@ResponseStatus(HttpStatus.CREATED)
	public void novaPessoa(@RequestBody Pessoa p) {
		pr.save(p);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable("id") int id) {
		pr.deleteById(id);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Pessoa> atualizar(@PathVariable int id, @Valid @RequestBody Pessoa p){
		Optional<Pessoa> salvar = pr.findById(id);
		BeanUtils.copyProperties(p, salvar.get(), "id");
		pr.save(salvar.get());
		return ResponseEntity.ok(salvar.get());
	}
}
