package com.crud.app.model;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class PessoaService {
	
	@Autowired
	PessoaRepository pr;
	
	public Pessoa Atualizar(int id, Pessoa p) {
		Pessoa salvar = buscaPessoaID(id);
		BeanUtils.copyProperties(p, salvar, "id");
		return pr.save(salvar);
	}

	private Pessoa buscaPessoaID(int id) {
		Optional<Pessoa> salvar = pr.findById(id);
		if(!salvar.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		return salvar.get();
	}
}
